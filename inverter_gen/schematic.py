import os
import pkg_resources

from bag.design import Module
#The module path should be global. Is it?
yaml_file = os.path.join(f'{os.environ["BAG_WORK_DIR"]}/BagModules/inverter_templates', 'netlist_info', 'inverter.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library inverter_templates cell inverter.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        Module.__init__(self, bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            lch='Channel length in meters', 
            tran_intent='Transistor threshold flavor',
            nmos_nf='Number of fingers of the NMOS transistor',
            nmos_w='Width in meters of the NMOS transistor',
            pmos_nf='Number of fingers of the PMOS transistor',
            pmos_w='Width in meters of the PMOS transistor',
            flip_well='Enable alternative strcuture for flipped well structures',
            sch_dummy_info='dummy data structure created by the layout generator'
        )

    def design(self, **kwargs):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """
        # Could define defaults here
        lch = kwargs.get('lch')
        tran_intent = kwargs.get('tran_intent')
        nmos_nf = kwargs.get('nmos_nf')
        nmos_w = kwargs.get('nmos_w')
        pmos_nf = kwargs.get('pmos_nf')
        pmos_w = kwargs.get('pmos_w')
        flip_well = kwargs.get('flip_well')
        sch_dummy_info = kwargs.get('sch_dummy_info')
        #print(sch_dummy_info)
        self.instances['Xnmos'].design(w=nmos_w, l=lch, intent=tran_intent, nf=nmos_nf)
        self.instances['Xpmos'].design(w=pmos_w, l=lch, intent=tran_intent, nf=pmos_nf)
        if flip_well:
            self.reconnect_instance_terminal('Xpmos','B','VSS',index=None)
            self.design_dummy_transistors(sch_dummy_info, inst_name='Xdummy', vdd_name='VSS', vss_name='VSS')
        else:
            self.design_dummy_transistors(sch_dummy_info, inst_name='Xdummy', vdd_name='VDD', vss_name='VSS')

        
